package qrpc

import (
	"bufio"
	"context"
	"crypto/tls"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"net"
	"reflect"
	"sync/atomic"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/qrpc/tracer"
	"bitbucket.org/fufoo/qrpc/wire"
	quic "github.com/quic-go/quic-go"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var prevClientID uint64 = 0
var trace = false

func SetTrace(flag bool) {
	trace = flag
}

type Client struct {
	clientID uint64
	secure   *tls.Config
	addr     string
	sess     quic.Connection
	maxFrame int
	perrpc   PerRPCCredentials
}

func (c *Client) logset(ctx context.Context) context.Context {
	ctx = logging.Set(ctx, "addr", c.addr)
	ctx = logging.Set(ctx, "cid", c.clientID)
	return ctx
}

const maxErrorLen = 32 * 1024

type DialOption func(*Client)

var ErrTLSRequired = errors.New("qrpc requires TLS")

type PerRPCCredentials interface {
	GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error)
}

func WithPerRPCCredentials(creds PerRPCCredentials) DialOption {
	panic("TBC")
	return func(c *Client) {
		c.perrpc = creds
	}
}

func With() DialOption {
	panic("TODO... figure out how gRPC does this")
	return func(c *Client) {
		//c.middlewares = append(c.middlewares, c)
	}
}

func DialWithTLS(t *tls.Config) DialOption {
	return func(c *Client) {
		c.secure = t
	}
}

func Dial(ctx context.Context, serverAddr string, opts ...DialOption) (*Client, error) {
	c := &Client{
		addr:     serverAddr,
		maxFrame: 2 * 1024 * 1024,
		clientID: atomic.AddUint64(&prevClientID, 1),
	}
	for _, opt := range opts {
		opt(c)
	}

	if c.secure == nil {
		return nil, ErrTLSRequired
	}

	err := c.reopen(ctx)
	if err != nil {
		return nil, err
	}
	return c, nil
}

func writeVarint(dst *bufio.Writer, n int64) error {
	var len [10]byte
	i := binary.PutVarint(len[:], n)
	_, err := dst.Write(len[:i])
	return err
}

func writeUvarint(dst *bufio.Writer, n uint64) error {
	var len [10]byte
	i := binary.PutUvarint(len[:], n)
	_, err := dst.Write(len[:i])
	return err
}

func writeString(dst *bufio.Writer, s string) error {
	err := writeUvarint(dst, uint64(len(s)))
	if err != nil {
		return err
	}

	_, err = dst.Write([]byte(s))
	return err
}

type RPCError struct {
	Code    int
	Message string
}

func (r *RPCError) Error() string {
	return fmt.Sprintf("%s (code %d)", r.Message, r.Code)
}

var ErrInvalidErrorEncoding = errors.New("invalid error encoding")

type grpcCoder interface {
	Code() codes.Code
}

func encodeError(err error) []byte {
	msg := err.Error()
	code := codes.Unknown

	if coder, ok := err.(grpcCoder); ok {
		code = coder.Code()
	}
	b := make([]byte, 20+len(msg))

	i := 0
	i += binary.PutVarint(b[i:], int64(code))
	i += binary.PutUvarint(b[i:], uint64(len(msg)))
	i += copy(b[i:], msg)
	return b[:i]
}

func parseErrorFrame(buf []byte) error {
	i := 0
	code, n := binary.Varint(buf[i:])
	if n <= 0 {
		return ErrInvalidErrorEncoding
	}
	i += n

	msglen, n := binary.Uvarint(buf[i:])
	if n <= 0 || msglen > maxErrorLen {
		panic(msglen)
		return ErrInvalidErrorEncoding
	}
	i += n

	if i+int(msglen) > len(buf) {
		return ErrInvalidErrorEncoding
	}

	return &RPCError{
		Code:    int(code),
		Message: string(buf[i : i+int(msglen)]),
	}
}

func (c *Client) reopen(ctx context.Context) error {
	qc := &quic.Config{
		Versions: []quic.VersionNumber{1},
		Tracer:   tracer.New(c.clientID),
	}
	session, err := quic.DialAddr(ctx, c.addr, c.secure, qc)
	if err != nil {
		return err
	}
	c.sess = session
	return nil
}

func (c *Client) retryableOpenStream(ctx context.Context) (quic.Stream, error) {
	var err error
	var stream quic.Stream

	for i := 0; i < 5; i++ {
		stream, err = c.sess.OpenStream()
		if err == nil {
			return stream, nil
		}

		neterr := err.(net.Error)
		if neterr.Timeout() {
			time.Sleep(20 * time.Millisecond)
			c.reopen(ctx)
		} else if neterr.Temporary() {
			time.Sleep(time.Duration(1<<i) * 10 * time.Millisecond)
			c.reopen(ctx)
		} else {
			break
		}
	}
	return stream, err
}

func (c *Client) RPC(ctx context.Context, method string, src, dst proto.Message) error {
	ctx = c.logset(ctx)
	stream, err := c.retryableOpenStream(ctx)
	if err != nil {
		log.Warningf(ctx, "could not open stream: %s", err)
		return err
	}

	defer stream.Close()

	tx := bufio.NewWriter(stream)

	err = writeString(tx, method)
	if err != nil {
		return err
	}
	buf, err := proto.Marshal(src)
	if err != nil {
		return err
	}

	err = writeUvarint(tx, uint64(len(buf)))
	if err != nil {
		return err
	}
	_, err = tx.Write(buf)
	if err != nil {
		return err
	}
	tx.Flush()

	// single (non-client-streaming) RPC, close after write
	stream.Close()

	// read the response
	rx := bufio.NewReader(stream)

	framelen, err := binary.ReadVarint(rx)
	if err != nil {
		log.Warningf(ctx, "could not read response: %s", err)
		return err
	}

	if framelen < 0 {
		framelen = -framelen
		// it denotes an error
		if framelen > maxErrorLen {
			panic("error too big")
		}
		errbuf := make([]byte, framelen)
		_, err = io.ReadFull(rx, errbuf)
		if err != nil {
			panic(err)
			return err
		}
		log.Tracef(ctx, "received %d byte error frame %.8x...", framelen, errbuf)

		err = parseErrorFrame(errbuf)
		log.Tracef(ctx, "got error: %s", err)
		return err
	} else {
		if framelen > int64(c.maxFrame) {
			panic("error too big")
		}
		msgbuf := make([]byte, framelen)
		_, err = io.ReadFull(rx, msgbuf)
		if err != nil {
			panic(err)
			return err
		}
		if trace {
			log.Tracef(ctx, "received %d byte frame %.8x...", framelen, msgbuf)
		}
		return proto.Unmarshal(msgbuf, dst)
	}
}

var EOF = io.EOF

func IsEOF(err error) bool {
	return errors.Is(err, EOF)
}

func IsCanceled(err error) bool {
	return errors.Is(err, context.Canceled)
}

func (c *Client) Keepalive(ctx context.Context, dt time.Duration) error {
	if dt < time.Second {
		panic("interval is too short")
	}

	ctx = c.logset(ctx)

	jitter := float64(dt) / 6
	for {
		delay := dt + time.Duration(jitter*rand.NormFloat64())
		if delay < time.Millisecond {
			continue
		}
		time.Sleep(delay)

		select {
		case <-ctx.Done():
			return context.Canceled
		default:
		}

		err := c.ping(ctx)
		if err != nil {
			log.Warningf(ctx, "ping failed: %s", err)
		}
	}
	return nil
}

func (c *Client) ping(ctx context.Context) error {
	var resp wire.PingResponse
	t0 := time.Now()
	err := c.RPC(ctx, "qrpc:///Ping", &wire.PingRequest{Sent: timestamppb.New(t0)}, &resp)
	if err != nil {
		return err
	}
	now := time.Now()
	t1 := resp.Rcvd.AsTime()
	if trace {
		log.Debugf(ctx, "there %s and back again %s", t1.Sub(t0), now.Sub(t1))
	}
	return nil
}

func (c *Client) RPCServerStream(ctx context.Context, method string, msg, dst proto.Message, ch chan<- interface{}) error {
	ctx = c.logset(ctx)

	// TODO fix duplicated initial bit of code with RPC()
	stream, err := c.retryableOpenStream(ctx)
	if err != nil {
		return err
	}
	defer stream.Close()

	tx := bufio.NewWriter(stream)

	err = writeString(tx, method)
	if err != nil {
		return err
	}
	buf, err := proto.Marshal(msg)
	if err != nil {
		return err
	}

	err = writeUvarint(tx, uint64(len(buf)))
	if err != nil {
		return err
	}
	_, err = tx.Write(buf)
	if err != nil {
		return err
	}
	tx.Flush()

	dtype := reflect.TypeOf(dst).Elem()

	// single (non-client-streaming) RPC, close after write
	stream.Close()

	go func() {
		<-ctx.Done()
		stream.CancelRead(1)
	}()

	go func() {
		defer close(ch)
		// read the response
		rx := bufio.NewReader(stream)

		for {
			framelen, err := binary.ReadVarint(rx)
			if err != nil {
				if err == io.EOF {
					return
				}
				var timeout *quic.IdleTimeoutError
				if errors.As(err, &timeout) {
					log.Tracef(ctx, "quicc timeout")
					return
				}
				log.Tracef(ctx, "quicc error: %s", err)
				return
			}

			if framelen < 0 {
				framelen = -framelen
				// it denotes an error
				if framelen > maxErrorLen {
					panic("error too big")
				}
				errbuf := make([]byte, framelen)
				_, err = io.ReadFull(rx, errbuf)
				if err != nil {
					panic(err)
					ch <- err
					return
				}
				log.Tracef(ctx, "received %d byte error frame on stream [%x]", framelen, errbuf)
				err = parseErrorFrame(errbuf)
				log.Tracef(ctx, "got error: %s", err)
				ch <- err
				return
			} else {
				if framelen > int64(c.maxFrame) {
					panic("error too big")
				}
				msgbuf := make([]byte, framelen)
				_, err = io.ReadFull(rx, msgbuf)
				if err != nil {
					panic(err)
					ch <- err
					return
				}
				dst0 := reflect.New(dtype).Interface().(proto.Message)
				//log.Tracef(ctx, "received %d byte frame on stream [%x]", framelen, msgbuf)
				err := proto.Unmarshal(msgbuf, dst0)
				if err != nil {
					ch <- err
					return
				}
				ch <- dst0
			}
		}
	}()
	return nil
}
