# Quic RPC

This is a simple implementation of RPC using
[Protobuf](https://en.wikipedia.org/wiki/Protocol_Buffers) over
[QUIC](https://en.wikipedia.org/wiki/QUIC).


## API

The Go API is intended to be similar to gRPC.  We do expose features
of quic (notably the QUIC session layer) that can be useful for certain
purposes such as long-lived connection state.

## Protocol

Sessions appear at the API layer in the form of middleware; all the RPC
protocol happens at the stream level.

When a client wants to make a request, it opens a stream on an
existing session.

Then it writes the name of the method as a length-prefixed string
(uvarint encoded length, followed by the bytes of the string).

The method name is `qrpc://<domain>/<service>/<method>` for example
`qrpc://fufoo.net/Storage/Load` is the method for the Load method
on the Storage service.

The server side is closed when there are no more objects to write; if
the RPC is client streaming, then subsequent messages are written.

The server then executes the method and writes the length of the
response with varint encoding.  A negative value is used for errors
(`-length`).

If the RPC is server streaming, then subsequent messages are written.

After writing all the messages the server closes the stream.

The server closes the stream immediately after returning an error, no
messages are sent after returning an error.



