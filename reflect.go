package qrpc

import (
	"bufio"
	"context"
	"fmt"
	"reflect"

	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/descriptorpb"
)

type reflectedInvocationSimpleHandler struct {
	fn   reflect.Value
	rcvr reflect.Value
	arg  reflect.Type
	ret  reflect.Type
}

type reflectedInvocationServerStreamingHandler struct {
	fn   reflect.Value
	rcvr reflect.Value
	arg  reflect.Type
	item reflect.Type
	rsbm reflect.Value // a function
}

func DispatchUsingDescriptorAndReflection(buf []byte, domain string, rcvr interface{}) ServerOption {

	var fd descriptorpb.FileDescriptorSet
	err := proto.Unmarshal(buf, &fd)
	if err != nil {
		panic(err)
	}

	r := reflect.ValueOf(rcvr)
	t := r.Type()
	index := make(map[string]MethodHandler)

	fmt.Printf("Loaded %d file descriptors\n", len(fd.File))
	for _, f := range fd.File {
		fmt.Printf("  including %q\n", *f.Name)
		for _, service := range f.Service {
			fmt.Printf("    contains service %q\n", *service.Name)
			for _, method := range service.Method {
				fmt.Printf("      - method %q\n", *method.Name)
				ss := method.ServerStreaming != nil && *method.ServerStreaming
				cs := method.ClientStreaming != nil && *method.ClientStreaming
				key := "qrpc://" + domain + "/" + *service.Name + "/" + *method.Name
				fmt.Printf("                  %s (%t %t)\n", key, cs, ss)

				m, ok := t.MethodByName(*method.Name)
				if !ok {
					panic(fmt.Sprintf("%T is missing method %q", rcvr, *method.Name))
				} else if !ss && !cs {
					// TODO validate the signature
					index[key] = &reflectedInvocationSimpleHandler{
						rcvr: r,
						fn:   m.Func,
						arg:  m.Type.In(2).Elem(),
						ret:  m.Type.Out(0),
					}
				} else if ss && !cs {
					streamInterface := m.Type.In(3)
					sendm, ok := streamInterface.MethodByName("Send")
					if !ok {
						panic("missing Send method")
					}
					item := sendm.Type.In(0)
					rsbm, ok := rsbRegistry[key]
					if !ok {
						panic("no reflected streamer maker")
					}
					index[key] = &reflectedInvocationServerStreamingHandler{
						rcvr: r,
						fn:   m.Func,
						arg:  m.Type.In(2).Elem(),
						item: item,
						rsbm: rsbm,
					}
				} else if cs && !ss {
					panic("unsupported client but not server streaming")
				} else {
					panic("unsupported client and server streaming")
				}
			}
		}
	}

	return func(c *Config) {
		for k, v := range index {
			c.dispatch[k] = v
		}
	}
}

func (r *reflectedInvocationServerStreamingHandler) Handle(ctx context.Context, on Stream, dst *bufio.Writer, recv []byte) error {
	// TODO common code prefix
	requestMsg := reflect.New(r.arg)
	err := proto.Unmarshal(recv, requestMsg.Interface().(proto.Message))
	if err != nil {
		panic("TODO")
	}
	log.Debugf(ctx, "request is {%s}", requestMsg.Interface())

	wrap := func(msg proto.Message) error {
		log.Debugf(ctx, "streaming back {%s}", msg)
		streamed, err := proto.Marshal(msg)
		if err != nil {
			panic("oops")
			return err
		}
		writeVarint(dst, int64(len(streamed)))
		_, err = dst.Write(streamed)
		if err != nil {
			panic("oops")
			return err
		}
		return dst.Flush()
	}

	streamer := r.rsbm.Call([]reflect.Value{reflect.ValueOf(wrap)})

	args := []reflect.Value{
		r.rcvr,
		reflect.ValueOf(ctx),
		requestMsg,
		streamer[0],
	}

	ret := r.fn.Call(args)

	if !ret[0].IsNil() {
		err := ret[1].Interface().(error)
		errorBuf := encodeError(err)
		writeVarint(dst, int64(-len(errorBuf)))
		dst.Write(errorBuf)
	}
	dst.Flush()
	return nil
}

var rsbRegistry = make(map[string]reflect.Value)

func RegisterReflectedServerBuilder(route string, fn interface{}) {
	rsbRegistry[route] = reflect.ValueOf(fn)
}

func (r *reflectedInvocationSimpleHandler) Handle(ctx context.Context, on Stream, dst *bufio.Writer, recv []byte) error {
	requestMsg := reflect.New(r.arg)
	err := proto.Unmarshal(recv, requestMsg.Interface().(proto.Message))
	if err != nil {
		panic("TODO")
	}

	ret := r.fn.Call([]reflect.Value{
		r.rcvr,
		reflect.ValueOf(ctx),
		requestMsg,
	})

	err = nil
	if !ret[1].IsNil() {
		err = ret[1].Interface().(error)
	}
	if err == nil {
		responseMsg := ret[0].Interface().(proto.Message)
		responseBuf, err := proto.Marshal(responseMsg)
		if err != nil {
			panic("TODO")
		}
		writeVarint(dst, int64(len(responseBuf)))
		dst.Write(responseBuf)
	} else {
		errorBuf := encodeError(err)
		writeVarint(dst, int64(-len(errorBuf)))
		dst.Write(errorBuf)
	}
	dst.Flush()
	return nil
}

func WriteResponse(ctx context.Context, dst *bufio.Writer, msg proto.Message, err error) error {
	var buf []byte
	var tag int

	if err == nil {
		buf, err = proto.Marshal(msg)
		if err != nil {
			panic(err)
			return err
		}
		if trace {
			log.Tracef(ctx, "writing non-error response of %d bytes", len(buf))
		}
		tag = len(buf)
	} else {
		buf = encodeError(err)
		log.Tracef(ctx, "writing error response of %d bytes", len(buf))
		tag = -len(buf)
	}
	writeVarint(dst, int64(tag))
	_, err = dst.Write(buf)
	dst.Flush()
	return err
}
