package qrpc

import (
	"bufio"
	"context"
	"encoding/binary"
	"io"
	"net"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/qrpc/wire"
	quic "github.com/quic-go/quic-go"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var log = logging.New("qrpc")

type Session interface {
	RemoteAddr() net.Addr
}

type Stream interface {
	Session() Session
}

type SessionMiddlewareFunc func(ctx context.Context, sess Session, next SessionHandler)
type SessionHandler func(ctx context.Context, sess Session)

type StreamHandler func(ctx context.Context, stream Stream)

type Config struct {
	h        SessionHandler
	s        StreamHandler
	maxFrame uint64
	dispatch map[string]MethodHandler
}

type ServerOption func(c *Config)

func SessionMiddleware(fn SessionMiddlewareFunc) ServerOption {
	return func(c *Config) {
		next := c.h
		c.h = func(ctx context.Context, sess Session) {
			fn(ctx, sess, next)
		}
	}
}

type Server struct {
	config Config
}

func NewServer(opts ...ServerOption) *Server {
	s := &Server{}
	s.config.h = s.accepter
	s.config.s = s.dispatcher
	s.config.maxFrame = 2 * 1024 * 1024
	s.config.dispatch = make(map[string]MethodHandler)

	s.config.dispatch["qrpc:///Ping"] = HandlerFunc(s.pingHandler)
	for _, opt := range opts {
		opt(&s.config)
	}
	return s
}

func (s *Server) pingHandler(ctx context.Context, stream Stream, dst *bufio.Writer, data []byte) error {

	resp := &wire.PingResponse{
		Rcvd: timestamppb.Now(),
	}
	return WriteResponse(ctx, dst, resp, nil)
}

type HandlerFunc func(context.Context, Stream, *bufio.Writer, []byte) error

func (fn HandlerFunc) Handle(ctx context.Context, stream Stream, dst *bufio.Writer, data []byte) error {
	return fn(ctx, stream, dst, data)
}

type MethodHandler interface {
	Handle(context.Context, Stream, *bufio.Writer, []byte) error
}

func (s *Server) RegisterServiceMethod(server interface{}, path string, h MethodHandler) {
	s.config.dispatch[path] = h
}

func (s *Server) Serve(ctx context.Context, lis *quic.Listener) error {
	for {
		sess, err := lis.Accept(ctx)
		if err != nil {
			log.Warningf(ctx, "failed to accept: %s", err)
			return err
		}
		log.Debugf(ctx, "started")
		conn := &clientConn{
			owner: s,
			sess:  sess,
		}
		go conn.serve(ctx)
	}
}

type clientConn struct {
	owner *Server
	sess  quic.Connection
}

type clientStream struct {
	owner  *clientConn
	stream quic.Stream
}

func (c *clientConn) RemoteAddr() net.Addr {
	return c.sess.RemoteAddr()
}

func (c *clientConn) serve(ctx context.Context) {
	// TODO backstop panic
	c.owner.config.h(ctx, c)
}

func (s *Server) accepter(ctx context.Context, sess Session) {
	raw := sess.(*clientConn).sess

	for {
		// log.Tracef(ctx, "accepting")
		stream, err := raw.AcceptStream(ctx)
		if err != nil {
			if ne, ok := err.(net.Error); ok {
				if ne.Timeout() {
					log.Debugf(ctx, "idle session shutting down")
					return
				}
			}
			log.Warningf(ctx, "%T %s", err, err)
			return
		}
		go s.config.s(ctx, &clientStream{
			owner:  sess.(*clientConn),
			stream: stream,
		})
	}
}

func (s *clientStream) Session() Session {
	return s.owner
}

// stream format:
//
//       path length       (varint)
//       path              []byte
//       frame length      (varint)
//       frame		   []byte
//
// the frame contains the protobuf encoded request object

func (s *Server) dispatcher(ctx context.Context, stream Stream) {
	raw := stream.(*clientStream).stream
	defer raw.Close()

	rx := bufio.NewReader(raw)

	pathlen, err := binary.ReadUvarint(rx)
	if err != nil {
		log.Debugf(ctx, "bad header read: %s", err)
		return
	}
	if pathlen > 1024 {
		log.Debugf(ctx, "bad header; path length %d is too big", pathlen)
		return
	}
	pathbuf := make([]byte, pathlen)
	_, err = io.ReadFull(rx, pathbuf)
	if err != nil {
		log.Debugf(ctx, "read failure")
		return
	}

	path := string(pathbuf)
	if trace {
		log.Tracef(ctx, "dispatching %q", path)
	}

	framelen, err := binary.ReadUvarint(rx)
	if err != nil {
		log.Debugf(ctx, "read failure")
		return
	}
	if trace {
		log.Debugf(ctx, "reading %d frame bytes", framelen)
	}
	if framelen > s.config.maxFrame {
		log.Debugf(ctx, "frame too big %d", framelen)
		return
	}

	h, ok := s.config.dispatch[path]
	if !ok {
		log.Debugf(ctx, "no handler for %q", path)
		// TODO unsupported method hook
		// TODO send back an error
		return
	}

	msgbuf := make([]byte, framelen)
	_, err = io.ReadFull(rx, msgbuf)
	if err != nil {
		log.Debugf(ctx, "read failure")
		return
	}

	tx := bufio.NewWriter(raw)

	h.Handle(ctx, stream, tx, msgbuf)
	// handler is responsible for writing the response
}
