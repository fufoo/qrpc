// this program generates the qrpc stubs for the services listed in a given
// protobuf descriptor
//
// The descriptor can be produced using something like this:
//
//     protoc --descriptor_set_out=lib/playapi/playapi.desc \
//            --go_out=. \
//            --go_opt=paths=source_relative \
//             lib/playapi/request.proto
//
// and the playapi.desc handed to this program, which produces an output file
// which contains interfaces for both client and server (in the current implementation,
// those interfaces are identical; in gRPC, by contrast, they differ slightly)
//
// the output file also contains a NewXxxClient(*qrpc.Client) which produces
// an implementation of an XxxClient

package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"text/template"

	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/descriptorpb"
)

func main() {
	domain := flag.String("domain", "", "Domain name")
	pkg := flag.String("package", "", "Name of output package")
	desc := flag.String("descriptor_set_in", "", "Path to proto descriptor file")
	outf := flag.String("go_out", "", "Where to write output file")
	flag.Parse()

	if *pkg == "" {
		panic("must specify -package")
	}

	buf, err := ioutil.ReadFile(*desc)
	if err != nil {
		panic(err)
	}

	var fd descriptorpb.FileDescriptorSet
	err = proto.Unmarshal(buf, &fd)
	if err != nil {
		panic(err)
	}

	// collect the services defined in the input descriptors

	var generate []Service

	for _, f := range fd.File {
		fmt.Printf("  including %q\n", *f.Name)
		for _, service := range f.Service {
			fmt.Printf("    contains service %q\n", *service.Name)

			svc := Service{
				Domain: *domain,
				Name:   *service.Name,
			}

			for _, method := range service.Method {
				fmt.Printf("      - method %q\n", *method.Name)
				ss := method.ServerStreaming != nil && *method.ServerStreaming
				cs := method.ClientStreaming != nil && *method.ClientStreaming
				//key := "qrpc://" + *domain + "/" + *service.Name + "/" + *method.Name

				if cs {
					panic("unsupported streaming")
				}

				m := Method{
					Name:            *method.Name,
					Arg:             localname(*method.InputType),
					Result:          localname(*method.OutputType),
					ServerStreaming: ss,
					ClientStreaming: cs,
				}
				svc.Methods = append(svc.Methods, m)
			}
			generate = append(generate, svc)
		}
	}

	t, err := template.New("service").Parse(serviceTemplate)
	if err != nil {
		panic(err)
	}

	out := bufio.NewWriter(os.Stdout)
	if *outf != "" {
		fd, err := os.Create(*outf)
		if err != nil {
			panic(err)
		}
		out = bufio.NewWriter(fd)
		defer fd.Close()
	}

	fmt.Fprintf(out, "// generated\n")
	fmt.Fprintf(out, "package %s\n", *pkg)
	fmt.Fprintf(out, "\n")
	fmt.Fprintf(out, "import (\n")
	fmt.Fprintf(out, "\t\"context\"\n")
	fmt.Fprintf(out, "\t\"bufio\"\n")
	fmt.Fprintf(out, "\n")
	fmt.Fprintf(out, "\t\"bitbucket.org/fufoo/qrpc\"\n")
	fmt.Fprintf(out, "\t\"google.golang.org/protobuf/proto\"\n")
	fmt.Fprintf(out, ")\n")

	/*
		svc := Service{
			Name: "PlayManager",
			Methods: []Method{
				{
					Name:   "Exec",
					Arg:    "Request",
					Result: "Response",
				},
				{
					Name:   "CreatePlay",
					Arg:    "CreatePlayRequest",
					Result: "CreatePlayResponse",
				},
			},
		}
		t.Execute(out, svc)
	*/

	for _, g := range generate {
		t.Execute(out, g)
	}

	out.Flush()
}

type Service struct {
	Domain  string
	Name    string
	Methods []Method
}

type Method struct {
	Name            string
	Arg             string
	Result          string
	ServerStreaming bool
	ClientStreaming bool
}

func localname(fq string) string {
	k := strings.LastIndexByte(fq, '.')
	if k < 0 {
		return fq
	}
	return fq[k+1:]
}

const serviceTemplate = `
{{ $service := . }}
type {{.Name}}QServer interface {
{{- range .Methods}}
{{- if .ServerStreaming}}
	{{.Name}}(context.Context, *{{.Arg}}) ({{$service.Name}}_{{.Name}}QClient, error)
{{- else}}
	{{.Name}}(context.Context, *{{.Arg}}) (*{{.Result}}, error)
{{- end}}
{{- end}}
}

{{- range .Methods}}
{{if .ServerStreaming}}
type {{$service.Name}}_{{.Name}}QClient interface {
	Recv() (*{{.Result}}, error)
}
{{- end}}
{{- end}}

type {{.Name}}QClient interface {
{{- range .Methods}}
{{- if .ServerStreaming}}
	{{.Name}}(context.Context, *{{.Arg}}) ({{$service.Name}}_{{.Name}}QClient, error)
{{- else}}
	{{.Name}}(context.Context, *{{.Arg}}) (*{{.Result}}, error)
{{- end}}
{{- end}}
}

type simple{{.Name}}Impl struct {
	rpc *qrpc.Client
}

func New{{.Name}}QClient(rpc *qrpc.Client) {{.Name}}QClient {
	return &simple{{.Name}}Impl{
		rpc: rpc,
	}
}

{{range .Methods}}
{{- if .ServerStreaming}}
func (c *simple{{$service.Name}}Impl) {{.Name}}(ctx context.Context, msg *{{.Arg}}) ({{$service.Name}}_{{.Name}}QClient, error) {
	var ret {{.Result}}
	ch := make(chan interface{}, 10)
	err := c.rpc.RPCServerStream(ctx, "qrpc://{{$service.Domain}}/{{$service.Name}}/{{.Name}}", msg, &ret, ch)
	if err != nil {
		return nil, err
	}
	return simple{{$service.Name}}{{.Name}}Streamer{ch}, nil
}

type simple{{$service.Name}}{{.Name}}Streamer struct {
	src <- chan interface{}
}

func (s simple{{$service.Name}}{{.Name}}Streamer) Recv() (*{{.Result}}, error) {
	item, ok := <- s.src
	if !ok {
		return nil, qrpc.EOF
	}
	if err, ok := item.(*qrpc.RPCError); ok {
		return nil, err
	}
	return item.(*{{.Result}}), nil
}

{{- else}}
func (c *simple{{$service.Name}}Impl) {{.Name}}(ctx context.Context, msg *{{.Arg}}) (*{{.Result}}, error) {
	var ret {{.Result}}
	err := c.rpc.RPC(ctx, "qrpc://{{$service.Domain}}/{{$service.Name}}/{{.Name}}", msg, &ret)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}
{{- end}}

type rx{{$service.Name}}_{{.Name}} struct {
	next {{$service.Name}}QServer
}

func (b rx{{$service.Name}}_{{.Name}}) Handle(ctx context.Context, _ qrpc.Stream, dst *bufio.Writer, recv []byte) error {
	var req {{.Arg}}
	
	err := proto.Unmarshal(recv, &req)
	if err != nil {
		return err
	}
	resp, err := b.next.{{.Name}}(ctx, &req)
{{if .ServerStreaming}}
	if err != nil {
		return qrpc.WriteResponse(ctx, dst, nil, err)
	}
        for {
		item, err := resp.Recv()
		if err != nil {
			if !qrpc.IsEOF(err) {
				return qrpc.WriteResponse(ctx, dst, nil, err)
			}
			return nil
		}
		qrpc.WriteResponse(ctx, dst, item, nil)
	}
{{else}}
	return qrpc.WriteResponse(ctx, dst, resp, err)
{{end}}
}

{{end}}

func Register{{.Name}}QServer(s *qrpc.Server, srv {{.Name}}QServer) {
{{- range .Methods}}
	s.RegisterServiceMethod(srv, "qrpc://{{$service.Domain}}/{{$service.Name}}/{{.Name}}", rx{{$service.Name}}_{{.Name -}} {srv})
{{- end}}
}

`
