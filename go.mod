module bitbucket.org/fufoo/qrpc

go 1.22

require (
	bitbucket.org/dkolbly/logging v0.9.4
	github.com/golang/protobuf v1.5.3
	github.com/quic-go/quic-go v0.40.1
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.28.0
)

require (
	github.com/go-task/slim-sprig v0.0.0-20230315185526-52ccab3ef572 // indirect
	github.com/google/pprof v0.0.0-20231212022811-ec68065c825e // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/onsi/ginkgo/v2 v2.13.2 // indirect
	github.com/quic-go/qtls-go1-20 v0.4.1 // indirect
	go.uber.org/mock v0.4.0 // indirect
	golang.org/x/crypto v0.17.0 // indirect
	golang.org/x/exp v0.0.0-20231219180239-dc181d75b848 // indirect
	golang.org/x/mod v0.14.0 // indirect
	golang.org/x/net v0.19.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
	golang.org/x/tools v0.16.1 // indirect
)
