package tracer

import (
	"context"
	"net"

	"bitbucket.org/dkolbly/logging"
	"github.com/quic-go/quic-go"
	. "github.com/quic-go/quic-go/logging"
)

var log = logging.New("quic")

func New(clientID uint64) func(context.Context, Perspective, quic.ConnectionID) *ConnectionTracer {
	return func(ctx context.Context, p Perspective, id quic.ConnectionID) *ConnectionTracer {
		ctx = logging.Set(ctx, "odcid", id)
		ctx = logging.Set(ctx, "cid", clientID)

		return &ConnectionTracer{
			StartedConnection: func(local, remote net.Addr, src, dst quic.ConnectionID) {
				log.Tracef(ctx, "started local={%s} remote={%s}", local, remote)
			},
			ClosedConnection: func(err error) {
				if err == nil {
					log.Tracef(ctx, "closed")
				} else {
					log.Tracef(ctx, "closed err=%s", err)
				}
			},
		}
	}
}
