#! /bin/bash

protoc \
    --go_out=. \
    --proto_path=. \
    --go_opt=paths=source_relative \
        wire.proto
